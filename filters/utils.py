#!/usr/bin/env python


def list_prefix(alist, prefix, sep=''):
    """Add prefix to each item of a string list"""
    return ['%s%s%s' % (prefix, sep, item) for item in alist]


def list_suffix(alist, suffix, sep=''):
    """Add suffix to each item of a string list"""
    return ['%s%s%s' % (item, sep, suffix) for item in alist]


def roles_to_groups(roles, env_name, sep=''):
    """
    Given a list of roles, get ansible groups.

    Used in os_server task, example:

    roles:
      - samba-common
      - samba-dc

    {{roles|roles_to_groups('env', sep='-')}}

    groups:
      - samba-common
      - samba-dc
      - env-samba-common
      - env-samba-dc

    We need the role names in groups, to make it easy to add group vars.
    But we also need the env prefixed groups, to limit tasks to current env.
    """
    return roles + list_prefix(roles, env_name, sep=sep)


class FilterModule(object):
    '''Ansible jinja2 filters'''

    def filters(self):
        return {
            'list_prefix': list_prefix,
            'list_suffix': list_suffix,
            'roles_to_groups': roles_to_groups,
        }
