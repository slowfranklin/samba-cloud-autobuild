#!/bin/bash
set -xue
apt-get update
apt-get install --yes python3-dev python3-setuptools python3-pip git
pip3 install -U -r requirements.txt
