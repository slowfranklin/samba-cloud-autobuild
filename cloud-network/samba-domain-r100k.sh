#!/bin/bash

set -xue

./samba-domain.yml -v \
    -e ENV_NAME=r100k \
    -e primary_dc_name=r100k-dc0 \
    -e samba_backup_file=$HOME/backup/samba-backup-vm-mdb-100000-max-100000-0531.tar.bz2 \
    "$@"
