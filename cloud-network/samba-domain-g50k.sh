#!/bin/bash

# create a samba domain and generate 125k users.

set -xue

ENV_NAME=g50k

./samba-domain.yml -v \
    -e ENV_NAME=${ENV_NAME} \
    -e primary_dc_name=${ENV_NAME}-dc0 \
    -e generate_users_and_groups=yes \
    -e num_users=50000 \
    -e num_max_members=50000 \
    "$@"
