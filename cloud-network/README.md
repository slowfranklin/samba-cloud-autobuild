# cloud-network

This ansible project aims to:
- build a image with packer
- create a network in a cloud, e.g.: openstack
- create required number of domain controllers
- provision first dc, and join the other ones
- create users, groups, group memberships as required
- can restore from a backup
