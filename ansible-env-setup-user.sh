#!/bin/bash
set -xue

/usr/local/bin/ansible-galaxy install --force -r requirements.yml
/usr/local/bin/ansible-galaxy list --verbose
$(dirname $0)/ansible-roles-clone-or-update.yml
