# Setup GitLab CI with Ansible in Cloud

This Ansible project will:

- create instance in cloud(openstack and rackspace so far)
- setup gitlab-runner in Docker with [autoscale mode](https://docs.gitlab.com/runner/executors/docker_machine.html)
- register gitlab-runner to specified gitlab instance

## Background Knowledge

- [GitLab Runner](https://docs.gitlab.com/runner/)
- Cloud: [OpenStack](https://www.openstack.org/)/[Rackspace](https://www.rackspace.com/)
- [Ansible](https://docs.ansible.com/)

## Virtualenv

Create a Python virutalenv with `../requirements.txt`, and activate it.
There are many ways to manage virtualenvs in Python, choose your own way.
Or you can just install to the system Python env:

    pip install --user -U -r ../requirements.txt

## Environment

You need credentials for your target cloud platform.

For OpenStack, download your OpenStack RC file and source it. To test:

    openstack server list
    ../inventory/openstack_inventory.py --list

For Rackspace ./run-rackspace.sh will work it out from this much:

    export RAX_USERNAME=<USERNAME>
    export RAX_API_KEY=<KEY>

If you want to use the tools manually, it's a bit mess. You need 3 groups of env vars for same thing:

    export RAX_USERNAME=<USERNAME>
    export RAX_API_KEY=<KEY>
    export RAX_REGION=DFW

    # used by rackspace cli app `rack`
    export RS_USERNAME=$RAX_USERNAME
    export RS_API_KEY=$RAX_API_KEY
    export RS_REGION_NAME=$RAX_REGION

    # used by docker-machine
    export OS_USERNAME=$RAX_USERNAME
    export OS_API_KEY=$RAX_API_KEY
    export OS_REGION_NAME=$RAX_REGION

Ansible will create ~/.rax_creds like this:

    [rackspace_cloud]
    username = <USERNAME>
    api_key = <API_KEY>

After that, you need to install `rack`(rackspace CLI app) in $PATH, which is a go binary.

Test:

    rack servers instance list
    ../inventory/rax.py --list

## SSH keypair

You will need a ssh keypair to login to the gitlab runnner cloud instance.
In Catalyst Samba Team, we use a shared common keypair which you can find in pview.
The current openstack and rackspace gitlab runner instances are both
provisioned with this keypair.

Further keys are listed in group_vars/all.yml so everyone does not need
to have the same private key.

## Vault password

Ask an existing maintainer for the vault password for to put in
~/.vault_password_samba_team
 and/or
~/.vault_password_catalyst

## Create runner

Then (will be named gitlab-runner-xxx-MMDD):

    ./run-rackspace-samba-team.sh
    ./run-openstack-catalyst.sh

or if you want a `version` number

    ./run-rackspace-samba-team.sh -e RUNNER_NAME=rackspace-runner-0
    ./run-openstack-catalyst.sh -e RUNNER_NAME=openstack-runner-0

## Upgrade runner

Notice the `0` in above example, that is the `version` for runner.
If you need to rebuild/upgrade the runner, we recommend build new one:

- increase the version number by 1 and create new runner
- pause the old runner via gitlab CI settings so it will not accept new jobs
- after all old jobs are finished, delete old runner

## Delete runner

    ./run-openstack.sh -e RUNNER_NAME=openstack-runner-0 -e state=absent
    ./run-rackspace.sh -e RUNNER_NAME=rackspace-runner-0 -e state=absent

## Debug

Sometimes the gitlab ci may stop to start new piplines, normally caused by
resource limit.
A common reason I know so far is that many executors run into an `ERROR`
status thus we run out of cloud quota, then docker-machine can not create
new instances.

To see running instances, use web dashboard or cli:

    openstack server list
    rack servers instance list

To delete the `ERROR` ones:

    openstack server delete ID
    rack servers instance delete ID

You may also need to check and delete the correspondent temp keypairs which
may also reach the quota limit:

    openstack keypair list
    rack servers keypair list

To test docker-machine:

Login to gitlab runner bastion host:

    ssh ubuntu@OPENSTACK-INSTANCE-IP
    ssh root@RACKSPACE-INSTANCE-IP

    ssh root@RACKSPACE-INSTANCE-IP

The authorised_keys loaded are in group_vars/all.yml

If the ansible script failed to compete (we remove this key on success
for security) and you created the instance but are not in the authorized_keys, then
you can use the key created during setup:
    ssh root@RACKSPACE-INSTANCE-IP -i ~/.ssh/id_$RUNNER_NAME

Please note the default user for ubuntu cloud image are different.

Login to docker container:

    docker exec -it gitlab-runner bash

Run docker-machine in the gitlab-runner docker container:

    docker-machine create testvm

We can do this without parameters becacause we already pass them as environment
variables to this docker contianer while start it. You can check them with `env`.

Normally the above command will give you the closest error message.

The easiest way to fix broken CI is:

- pause the current broken runner
- delete all instances in `ERROR` status and related keypairs so we release quota
- create new runner with a increased version number
- delete the old runner

