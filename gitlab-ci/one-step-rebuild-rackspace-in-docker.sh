#!/bin/bash -xe
old_umask=$(umask)
umask 077
echo $VAULT_PW > ~/.vault_password_samba_team
umask $old_umask
cd src/
./ansible-env-setup-root.sh
./ansible-env-setup-user.sh
./gitlab-ci/run-rackspace-samba-team.sh
