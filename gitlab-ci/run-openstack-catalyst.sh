#!/bin/bash -x
# You still need the vault password added in ~/.vault_password_catalyst

export ANSIBLE_INVENTORY=../inventory/openstack_inventory.py

$(dirname $0)/openstack_catalyst.yml -v \
    --vault-password-file ~/.vault_password_catalyst \
    -e ENV_NAME=gitlab-runner \
    -e MACHINE_DRIVER=openstack \
    -e RUNNER_NAME=gitlab-runner-openstack-$(date +%m%d) \
    "$@"
