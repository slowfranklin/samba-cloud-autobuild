#!/usr/bin/env python3
import json
import smtplib
import argparse
import subprocess
from os import getenv
from io import StringIO
from datetime import datetime, timedelta

import logging
log = logging.getLogger('cloud-checker')


def json_dumps(obj):
    return json.dumps(obj, indent=4, sort_keys=True)


def get_cmd_output(cmd, as_json=True, dry_run=False):
    log.info('running cmd: \n\n%s\n\n', cmd)
    if dry_run:
        return
    output = subprocess.check_output(cmd.split()).decode('utf-8').strip()
    log.debug('cmd output: \n\n%s\n\n', output)
    if output and as_json:
        output = json.loads(output)
    return output


def send_email(report):
    host = getenv('EMAIL_HOST')
    port = getenv('EMAIL_PORT')
    assert host and port, 'you must specify $EMAIL_HOST and $EMAIL_PORT'

    username = getenv('EMAIL_USERNAME')
    password = getenv('EMAIL_PASSWORD')
    assert username and password, 'you must specify $EMAIL_USERNAME and $EMAIL_PASSWORD'

    from_ = getenv('EMAIL_FROM', default=username)
    to = getenv('EMAIL_TO', default=username)

    smtp = smtplib.SMTP_SSL(host, int(port))
    smtp.login(username, password)

    email_message = "From: {}\r\nTo: {}\r\nSubject:{}\r\n\r\n{}".format(
        from_, to, '[samba-cloud-checker] Reminder for costly Cloud resources', report)
    log.debug('email message: \n\n%s\n\n', email_message)
    smtp.sendmail(from_, to.split(','), email_message)
    smtp.quit()


class Cloud(object):
    # cmd to list server
    cmd_server_list = ''
    # output of above cmd
    out_server_list = None  # None means we didn't fetch it yet
    # cmd to get server info
    cmd_server_get = ''
    # key for created time in server get cmd response
    key_server_created = ''

    def __init__(self, output=None, delete=False):
        self.report = output or StringIO()
        self.delete = delete  # auto delete long live runners

    def strptime(self, s, fmt="%Y-%m-%dT%H:%M:%SZ"):
        # both clouds have same format
        return datetime.strptime(s, fmt)

    def get_hours(self, dt, now=None):
        now = now or datetime.utcnow()  # api return UTC time
        delta = now - dt
        seconds = delta.total_seconds()  # float
        return seconds/3600.0

    def filter_servers(self, servers):
        suspects = []
        for server in servers:
            if server['Status'] == 'SHELVED_OFFLOADED':
                continue
            if server['Name'].startswith('gitlab-runner'):
                continue
            name = server['Name']
            cmd = self.cmd_server_get.format(name=name)
            server_info = get_cmd_output(cmd)
            str_created = server_info.get(self.key_server_created)
            if not str_created:
                continue
            created = self.strptime(str_created)
            hours = self.get_hours(created)
            server["Created"] = str_created
            server["Age"] = "{}+ hours".format(int(hours))
            if name.startswith('runner-'):
                # for runner not too old, that's fine
                if hours < 10.0:
                    continue
                elif self.delete:  # long live runner and delete required
                    log.warn('deleting long live instance: %s, %d+ hours', name, int(hours))
                    cmd = self.cmd_server_delete.format(name=name)
                    get_cmd_output(cmd, as_json=False)
                    server['Deleted'] = 'Yes'
            suspects.append(server)
        return suspects

    def check_servers(self):
        servers = get_cmd_output(self.cmd_server_list)
        # keep it on self, we need it again in cleanup_keypairs
        self.out_server_list = servers
        if servers:
            servers = self.filter_servers(servers)
            if servers:
                report = '\n\nUnexpected servers in {}: \n\n{}\n\n'.format(self.__class__.__name__, json_dumps(servers))
                self.report.write(report)

    def cleanup_keypairs(self):
        """
        Cleanup hanging runner keypairs.

        Although we delete hanging runners, but its keypair is still hanging,
        which may hit the quote limit for keypairs.
        """
        if self.out_server_list is None:
            log.warning('server list is not fetched yet, skip cleanup keypairs')
            return
        server_name_set = {server['Name'].strip() for server in self.out_server_list}

        keypairs = get_cmd_output(self.cmd_keypair_list, as_json=False)
        if not keypairs:
            log.warning('no keypairs found, skip cleanup keypairs')
            return

        # example keypair name:
        # runner-7x6ez4ez-1554436881-ac231983-21c839c205053907453908a32834eae3fe05a59525a9db51656012badf1ce43f
        # in which instance name is:
        # runner-7x6ez4ez-1554436881-ac231983
        for line in keypairs.strip().splitlines():
            if line.startswith('runner-'):  # we only care runner so far
                keypair_name = line.strip()
                server_name = keypair_name.rsplit(sep='-', maxsplit=1)[0]
                if server_name not in server_name_set:
                    log.warning('server %s gone, rm keypair %s', server_name, keypair_name)
                    cmd = self.cmd_keypair_delete.format(name=keypair_name, as_json=False)
                    get_cmd_output(cmd, as_json=False)


class Openstack(Cloud):
    cmd_server_list = 'openstack server list -c ID -c Name -c Status -c Flavor --print-empty --format json'
    cmd_server_get = 'openstack server show --format json {name}'
    cmd_server_delete = 'openstack server delete --wait {name}'
    cmd_keypair_list = 'openstack keypair list  --format value -c Name'
    # no output if succeed
    cmd_keypair_delete = 'openstack keypair delete {name}'
    key_server_created = 'created'


class Rackspace(Cloud):
    cmd_server_list = 'rack servers instance list --output json --fields id,name,status'
    cmd_server_get = 'rack servers instance get --output json --name {name}'
    cmd_server_delete = 'rack servers instance delete --wait-for-completion --name {name}'
    cmd_keypair_list = 'rack servers keypair list --fields name'
    cmd_keypair_delete = 'rack servers keypair delete --name {name}'
    key_server_created = 'Created'


def main():
    parser = argparse.ArgumentParser(description='Openstack & Rackspace Cloud Checker')
    parser.add_argument('--send-email', dest="send_email", action='store_true', default=False, help='send check result via email after check')
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Print verbose log')
    parser.add_argument('-d', '--delete', action='store_true', default=False, help='Auto delete long live runners')
    parser.add_argument(
        '--cloud', choices=['all', 'openstack', 'rackspace'], default='all',
        help='Which cloud to target at')
    args = parser.parse_args()

    level = 'DEBUG' if args.verbose else 'WARNING'
    logging.basicConfig(level=level)

    output = StringIO()
    clouds = []
    if args.cloud in ['all', 'rackspace']:
        cloud = Rackspace(output=output, delete=args.delete)
        clouds.append(cloud)
    if args.cloud in ['all', 'openstack']:
        cloud = Openstack(output=output, delete=args.delete)
        clouds.append(cloud)
    for cloud in clouds:
        cloud.check_servers()
        cloud.cleanup_keypairs()
    result = output.getvalue()
    if result:
        log.info(result)
        if args.send_email:
            send_email(result)


if __name__ == '__main__':
    main()
