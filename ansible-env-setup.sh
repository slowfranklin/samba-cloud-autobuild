#!/bin/bash
set -xue

sudo $(dirname $0)/ansible-env-setup-root.sh
$(dirname $0)/ansible-env-setup-user.sh
